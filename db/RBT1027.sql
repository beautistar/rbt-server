/*
SQLyog Ultimate v11.3 (64 bit)
MySQL - 5.7.20-0ubuntu0.16.04.1 : Database - rbt
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rbt` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `rbt`;

/*Table structure for table `tb_alert` */

DROP TABLE IF EXISTS `tb_alert`;

CREATE TABLE `tb_alert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `full_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `alert_type` int(2) NOT NULL COMMENT '1:police,2:emergency,3:fire',
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` double(15,8) NOT NULL,
  `longitude` double(15,8) NOT NULL,
  `sub_location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `members` int(11) NOT NULL,
  `recorded_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_alert` */

insert  into `tb_alert`(`id`,`user_id`,`full_name`,`alert_type`,`location`,`latitude`,`longitude`,`sub_location`,`members`,`recorded_time`) values (41,12,'Yin',2,'Union Square',37.78583400,-122.40641700,'Stockton St',5,'2017-10-26 18:49:32'),(44,12,'Yin',1,'Tie Nan Lu',42.89439528,129.56068853,'Yanji Shi',13,'2017-10-26 13:18:29'),(45,12,'YinFeng Piao',3,'null',-33.86881833,151.20929000,'null',3,'2017-10-26 13:59:33');

/*Table structure for table `tb_user` */

DROP TABLE IF EXISTS `tb_user`;

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fb_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cur_lat` float(15,8) NOT NULL,
  `cur_lng` float(15,8) NOT NULL,
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_user` */

insert  into `tb_user`(`id`,`fb_id`,`full_name`,`email`,`password`,`cur_lat`,`cur_lng`,`reg_date`) values (1,'1111','aaaa','','',222.22200012,333.33300781,'2017-10-22 05:13:19'),(2,'12312312312','AAAAA','','',221.11109924,222.22221375,'2017-10-22 09:34:00'),(3,'','AAAAA','test1@gmail.com','$2y$10$IXi1s5Oa6Ye4miNwrC9IbOgdeMOYtlmK1FYOW3VidSklYCVvRB60W',221.11109924,222.22221375,'2017-10-24 09:34:42'),(4,'123123123','AAAAA','','',221.11109924,222.22221375,'2017-10-24 09:45:31'),(5,'','AAAAA','test2@gmail.com','$2y$10$iLtV.FzDy83eK3VgLFsCvOmoC2dVFWF4ybZFHc///OcDQhOTRa7.O',221.11109924,222.22221375,'2017-10-24 13:32:50'),(10,'','TopTester','toptester@aq.com','$2y$10$XGYmrrL/Sddi2e7yFU/2CesLM7c3XPi4jC0MbwJFQM0jHYDzi68oe',0.00000000,0.00000000,'2017-10-25 03:33:23'),(11,'19292929292929','test','','',42.89433670,129.56068420,'2017-10-25 09:58:50'),(12,'504724896555224','YinFeng Piao','','',37.78583527,-122.40641785,'2017-10-25 10:03:27'),(13,'','qw','q@a.com','$2y$10$nkTHmmtHQtdqj/7VZxa/DuqWI/VhHk7v3gVDgFcjeBsvVH2RQqSRe',0.00000000,0.00000000,'2017-10-25 12:28:14'),(14,'','AAAAA3','test3@gmail.com','$2y$10$qhuiL/AlcTZpbfvFeWfRauuDuy9CGOQfuabBEQ.nbUtlP3xnNRn6G',221.11109924,222.22221375,'2017-10-25 18:34:25'),(15,'','qqq qqq','qqq@qqq.qqq','$2y$10$KM9GGR1wx8sViI1dhZHr8eGGVYT2/99jaLYHFnxgyuasDyIIX5elG',37.78583527,-122.40641785,'2017-10-25 18:38:42'),(16,'','aaa aaaa','aaa@aaa.aaa','$2y$10$6dR.gqClXDGqgjf5MvxHceNxRd0zJ8UxCWPyrz/pafVEj2RGJXS/W',37.78583527,-122.40641785,'2017-10-25 18:56:22'),(17,'','zzzzzz','zzz@zzz.com','$2y$10$HRfHkuV4S4eCvmgk8fURCOEgu2QKgIwU.HU5xZj.wGb.psrxOr8Om',0.00000000,0.00000000,'2017-10-26 03:36:11'),(18,'12345786322','mmmmm','','',42.01539993,129.84574890,'2017-10-26 03:42:36'),(19,'503366500027833','ZhandongDu','','',0.00000000,0.00000000,'2017-10-26 08:56:11'),(20,'123456','test123@gmail.com','','',129.02450562,49.01449966,'2017-10-26 08:58:57'),(21,'','bs','c','$2y$10$7/Iac0YVU/qARXjbyQooQefOh0k2j5/btsKck1Rx/p8rX6RDJJdfC',0.00000000,0.00000000,'2017-10-26 11:17:32'),(22,'','b k','bradleys@hotmail.com.au','$2y$10$.rY7makTd1uCtXmw4jp0l.D0dhodZU75HcHGxq2ep.D4uRw8OLUTm',-12.51813412,131.06248474,'2017-10-26 11:37:53');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
