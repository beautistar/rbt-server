/*
SQLyog Ultimate v11.3 (64 bit)
MySQL - 5.7.19-0ubuntu0.16.04.1 : Database - rbt
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rbt` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `rbt`;

/*Table structure for table `tb_alert` */

DROP TABLE IF EXISTS `tb_alert`;

CREATE TABLE `tb_alert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `full_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `alert_type` int(2) NOT NULL COMMENT '1:police,2:emergency,3:fire',
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` double(15,8) NOT NULL,
  `longitude` double(15,8) NOT NULL,
  `members` int(11) NOT NULL,
  `recorded_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_alert` */

insert  into `tb_alert`(`id`,`user_id`,`full_name`,`alert_type`,`location`,`latitude`,`longitude`,`members`,`recorded_time`) values (3,1111,'1111',1,'aaa',222.22200012,222.22200012,1,'2017-10-22 01:17:43'),(4,1111,'1111',1,'aaa',222.22200012,222.22200012,1,'2017-10-22 07:17:46'),(5,1111,'1111',1,'aaa',222.22200012,222.22200012,1,'2017-10-22 08:05:42'),(6,1111,'1111',1,'aaa',222.22200012,222.22200012,1,'2017-10-22 08:06:25'),(7,1111,'1111',1,'aaa',222.22200012,333.33300781,4,'2017-10-22 08:10:31'),(8,1111,'1111',1,'aaa',222.22200012,333.32998657,1,'2017-10-22 08:10:37'),(9,1,'1',1,'aaa',222.22200012,333.32998657,1,'2017-10-22 08:13:26'),(10,1,'1',1,'aaa',222.22200012,333.32998657,1,'2017-10-22 08:35:54'),(11,1,'1',1,'aaa',222.22200012,333.32998657,1,'2017-10-22 08:35:58'),(12,1,'1',1,'aaa',222.22200000,333.33000000,1,'2017-10-22 08:37:01'),(13,1,'1',1,'aaa',222.22200000,333.33000000,1,'2017-10-22 08:37:05'),(14,1,'1',1,'aaa',222.22200000,333.30000000,2,'2017-10-22 08:42:28'),(15,1,'1',1,'aaa',222.22200000,333.00000000,2,'2017-10-22 08:43:06');

/*Table structure for table `tb_user` */

DROP TABLE IF EXISTS `tb_user`;

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fb_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cur_lat` float(15,8) NOT NULL,
  `cur_lng` float(15,8) NOT NULL,
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_user` */

insert  into `tb_user`(`id`,`fb_id`,`full_name`,`cur_lat`,`cur_lng`,`reg_date`) values (1,'1111','aaaa',222.22200012,333.33300781,'2017-10-22 05:13:19');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
