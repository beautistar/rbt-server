<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller  {


    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('api_model');
        $this->load->library('session');     
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

     private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
     }

     /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

     private function doRespondSuccess($result){

         $this->doRespond(0, $result);
     }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

     private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
     } 
     
     function register() {
         
         $result = array();         
         $full_name = $_POST['full_name'];
         $email = $_POST['email'];
         $password = $_POST['password'];
         $cur_lat = $_POST['cur_lat'];
         $cur_lng = $_POST['cur_lng'];         
         
         $email_exist = $this->api_model->email_exist($email);
         
         $result['msg'] = "User already registered";
         if ($email_exist > 0) {
             $this->doRespond(101, $result);
             return;
         }
         
         if(!function_exists('password_hash'))
             $this->load->helper('password');

         $password_hash = password_hash($password, PASSWORD_BCRYPT);
         
         $result['msg'] = "User successfully registered";
         $result['user_id'] = $this->api_model->register($full_name, $email, $password_hash, $cur_lat, $cur_lng);
         $this->doRespondSuccess($result);
     }
     
     function login() {
         
         $result = array();  
         $email = $_POST['email'];
         $password = $_POST['password'];
         
         $email_exist = $this->api_model->email_exist($email);
         
         if ($email_exist == 0) {
             $result['msg'] = "User doesn't registered";
             $this->doRespond(101, $result);
             return;
         }
         
         if(!function_exists('password_verify'))
             $this->load->helper('password'); // password_helper.php loading

         $row = $this->db->get_where('tb_user', array('email'=>$email))->row();
         $pwd = $row->password;

         if (!password_verify($password, $pwd)){  // wrong password.
             
             $result['msg'] = "Wrong password";
             $this->doRespond(102, $result);
             return;

         } else {
             
             $result['user_info'] = array('user_id' => $row->id,
                                          'full_name' => $row->full_name
                                          );                         
         }

         $this->doRespondSuccess($result);  
         
     }
     
     function fbLogin() {
         
         $result = array();         
         $full_name = $_POST['full_name'];
         $fb_id = $_POST['fb_id'];
         $cur_lat = $_POST['cur_lat'];
         $cur_lng = $_POST['cur_lng'];
         
         $fb_exist = $this->api_model->fb_exist($fb_id);
         
         if ($fb_exist > 0) {
             
             $row = $this->db->get_where('tb_user', array('fb_id'=>$fb_id))->row();
             $result['user_info'] = array('user_id' => $row->id,
                                          'full_name' => $row->full_name
                                          ); 
         } else {
             $user_id = $this->api_model->fb_login($fb_id, $full_name, $cur_lat, $cur_lng);
             $result['user_info'] = array('user_id' => $user_id,
                                          'full_name' => $full_name
                                          ); 
         }
         
         $this->doRespondSuccess($result);
     }
     
     function updateLocation(){
         
         $result = array();
         
         $user_id = $_POST['user_id'];
         $cur_lat = $_POST['cur_lat'];
         $cur_lng = $_POST['cur_lng'];
         
         $this->api_model->updateLocation($user_id, $cur_lat, $cur_lng);
         
         $this->doRespondSuccess($result);
     }
     
     function setAlert() {
         
         $result = array();
         $user_id = $_POST['user_id'];
         $full_name = $_POST['full_name'];
         $latitude = $_POST['cur_lat'];
         $longitude = $_POST['cur_lng'];
         $alert_type = $_POST['type'];
         $location = $_POST['location'];
         $sub_location = $_POST['sub_location'];
         
         $result = $this->api_model->setAlert($user_id, $full_name, $latitude, $longitude, $alert_type, $location, $sub_location);
         $this->doRespondSuccess($result);
     }
     
     // find alert list within radius 50Km
     function getAlertList() {
         
         $result = array();
         
         $this->api_model->clearAlert();
         
         $user_id = $_POST['user_id'];         
         $latitude = $_POST['cur_lat'];
         $longitude = $_POST['cur_lng'];
         $distance = 50;
         $result['alert_list'] = $this->api_model->getAlertList($user_id, $latitude, $longitude, $distance);         
         
         $this->doRespondSuccess($result);
     }
     
     // find alert within radius 2Km
     function findAlertMap() {
         
         $result = array();
         
         $this->api_model->clearAlert();
         
         $user_id = $_POST['user_id'];         
         $latitude = $_POST['cur_lat'];
         $longitude = $_POST['cur_lng'];
         
         $result['alert_list'] = $this->api_model->findAlertMap($user_id, $latitude, $longitude);         
         
         $this->doRespondSuccess($result);
     }
     
     function versionInfo() {
         
         phpinfo();
     } 
     
     function getAlerts() {
         
         $result = array();
         
         $this->api_model->clearAlert();
         
         $user_id = $_POST['user_id'];         
         $distance = $_POST['distance'];         
         $latitude = $_POST['cur_lat'];
         $longitude = $_POST['cur_lng'];
         
         $result['alert_list'] = $this->api_model->getAlertList($user_id, $latitude, $longitude, $distance);         
         
         $this->doRespondSuccess($result);
     }                       
}

?>
