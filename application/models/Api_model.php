<?php

class Api_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }


    function fb_exist($fb_id) {
        
        $this->db->where('fb_id', $fb_id);
        if ($this->db->get('tb_user')->num_rows() > 0) {
            
            return 1;
        } else {
            return 0;
        }
    }
    
    function email_exist($email) {
        
        $this->db->where('email', $email);
        if ($this->db->get('tb_user')->num_rows() > 0) {
            
            return 1;
        } else {
            return 0;
        }
    }
    
    function fb_login($fb_id, $full_name, $cur_lat, $cur_lng) {
        
        $this->db->set('full_name', $full_name);
        $this->db->set('fb_id', $fb_id);
        $this->db->set('cur_lat', $cur_lat);
        $this->db->set('cur_lng', $cur_lng);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_user');
        return $this->db->insert_id();
    }
    
    function register($full_name, $email, $password_hash, $cur_lat, $cur_lng) {
        
        $this->db->set('full_name', $full_name);
        $this->db->set('email', $email);
        $this->db->set('password', $password_hash);
        $this->db->set('cur_lat', $cur_lat);
        $this->db->set('cur_lng', $cur_lng);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_user');
        return $this->db->insert_id();
    }
    
    function updateLocation($user_id, $cur_lat, $cur_lng) {
        
        $this->db->where('id', $user_id);
        $this->db->set('cur_lat', $cur_lat);
        $this->db->set('cur_lng', $cur_lng);
        $this->db->update('tb_user');
    }
    
    function setAlert($user_id, $full_name, $latitude, $longitude, $alert_type, $location, $sub_location) {
        
        $result = array();
        
        $sql = "SELECT tb_alert.*, tbl2.distance AS distance                        
                        FROM ( SELECT id, distance
                               FROM ( SELECT id,
                                    DEGREES(ACOS(
                                        COS(RADIANS(?)) * COS( RADIANS(latitude)) * COS(RADIANS(?) - RADIANS(longitude)) +
                                        SIN(RADIANS(?)) * SIN( RADIANS(latitude)) )
                                        ) * 60 * 1.1515 * 1.609344 as distance
                                    FROM tb_alert
                                    ) tb_distance                                    
                                   WHERE distance <= 0.5) tbl2, tb_alert                        
                        WHERE tbl2.id = tb_alert.id
                        GROUP BY tb_alert.id                         
              ";
        $sql_params = array($latitude, $longitude, $latitude);
        $query = $this->db->query($sql, $sql_params);
        
        if ($query->num_rows() > 0) {
            
            foreach ($query->result() as $alert) {
            
                $alert_id = $alert->id;
                $this->db->where('id', $alert_id);
                if ($user_id !=  $alert->user_id) {
                    $this->db->set('members', 'members + 1', FALSE);
                }
                $this->db->set('location', $location);
                $this->db->set('sub_location', $sub_location);
                $this->db->set('alert_type', $alert_type);
                $this->db->update('tb_alert');
                $result['distance'] = $query->row()->distance; 
            }
            $result['msg'] = "Already alerted area.";
            return $result;
        }
        
        $this->db->set('user_id', $user_id);
        $this->db->set('full_name', $full_name);
        $this->db->set('latitude', $latitude);
        $this->db->set('longitude', $longitude);
        $this->db->set('alert_type', $alert_type);
        $this->db->set('members', 1);
        $this->db->set('location', $location);
        $this->db->set('sub_location', $sub_location);
        $this->db->set('recorded_time', 'NOW()', false);
        $this->db->insert('tb_alert');        
        $result['alert_id'] = $this->db->insert_id(); 
        $result['msg'] = "New alert reported";
        return $result;
    }
    
    function clearAlert() {
        
        $sql = "DELETE FROM tb_alert WHERE recorded_time < NOW() - INTERVAL 8 HOUR";
        $query = $this->db->query($sql);        
    }
    
    function getAlertList($user_id, $lat, $lng, $distance) {
        
        $result = array();
        
        $sql = "SELECT tb_alert.*, tbl2.distance AS distance                        
                        FROM ( SELECT id, distance
                               FROM ( SELECT id,
                                    DEGREES(ACOS(
                                        COS(RADIANS(?)) * COS( RADIANS(latitude)) * COS(RADIANS(?) - RADIANS(longitude)) +
                                        SIN(RADIANS(?)) * SIN( RADIANS(latitude)) )
                                        ) * 60 * 1.1515 * 1.609344 as distance
                                    FROM tb_alert
                                    ) tb_distance";
                                    if ($distance > 0) $sql.=" WHERE distance <= " . $distance;
                                  
                               $sql.=") tbl2, tb_alert";                                    
                                   
                        $sql.=" WHERE tbl2.id = tb_alert.id AND tb_alert.user_id != $user_id
                        GROUP BY tb_alert.id
                        ORDER BY tbl2.distance ASC, tb_alert.recorded_time DESC 
         ";
         $sql_params = array($lat, $lng, $lat);
         $query = $this->db->query($sql, $sql_params);
         
         if ($query->num_rows() > 0) {
             foreach($query->result() as $alert) {
                 
                 $arr = array('id' => $alert->id,
                              'full_name' => $alert->full_name,
                              'user_id' => $alert->user_id,
                              'alert_type' => $alert->alert_type,
                              'location' => $alert->location,
                              'sub_location' => $alert->sub_location,
                              'latitude' => $alert->latitude,
                              'longitude' => $alert->longitude,
                              'members' => $alert->members,
                              'record_time' => $alert->recorded_time,
                              'distance' => $alert->distance);
                 array_push($result, $arr);
             }
         }
         
         return $result;
    }
    
    function findAlertMap($user_id, $lat, $lng) {
        
        $result = array();
        
        $sql = "SELECT tb_alert.*, tbl2.distance AS distance                        
                        FROM ( SELECT id, distance
                               FROM ( SELECT id,
                                    DEGREES(ACOS(
                                        COS(RADIANS(?)) * COS( RADIANS(latitude)) * COS(RADIANS(?) - RADIANS(longitude)) +
                                        SIN(RADIANS(?)) * SIN( RADIANS(latitude)) )
                                        ) * 60 * 1.1515 * 1.609344 as distance
                                    FROM tb_alert
                                    ) tb_distance                                    
                                   WHERE distance <= 2) tbl2, tb_alert
                        
                        WHERE tbl2.id = tb_alert.id  AND tb_alert.user_id != $user_id
                        GROUP BY tb_alert.id
                        ORDER BY tbl2.distance ASC, tb_alert.recorded_time DESC 
         ";
         $sql_params = array($lat, $lng, $lat);
         $query = $this->db->query($sql, $sql_params);
         
         if ($query->num_rows() > 0) {
             foreach($query->result() as $alert) {
                 
                 $arr = array('id' => $alert->id,
                              'full_name' => $alert->full_name,
                              'user_id' => $alert->user_id,
                              'alert_type' => $alert->alert_type,
                              'location' => $alert->location,
                              'sub_location' => $alert->sub_location,
                              'latitude' => $alert->latitude,
                              'longitude' => $alert->longitude,
                              'members' => $alert->members,
                              'record_time' => $alert->recorded_time,
                              'distance' => $alert->distance);
                 array_push($result, $arr);
             }
         }
         
         return $result;
    }
}
?>
